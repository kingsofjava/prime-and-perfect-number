package sample;

//import javafx.application.Application;
//import javafx.fxml.FXMLLoader;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import javafx.stage.Stage;

import static sample.Controller.*;

//public class Main extends Application {
public class Main  {

//    @Override
//    public void start(Stage primaryStage) throws Exception{
//        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
//        primaryStage.setTitle("Hello World");
//        primaryStage.setScene(new Scene(root, 500, 500));
//        primaryStage.show();
//    }


    public static void main(String[] args) {
//        launch(args);

        new Main();
    }

    private Main() {
        int start = 0;
        int end = 30;

//        System.out.println("Range From: " + start + " - " + end);

        int count = 0;
        for (int i = start; i <= end; i++) {
            count++;
//            System.out.println("i: " + i);

            String answer = i + "";
            if (isNumberPrime(i)) {
                answer = answer + " is prime";
            }

            if (isNumberPerfect(i)) {
                if (!answer.contains("is prime")) answer = answer + " is perfect";
                else answer = answer + " and perfect";
            }

//            if (count != 1 && isNumberAmicable(i, i - 1)) {
//                int prevNumber = i - 1;
//                if (answer.matches("[0-9]+")) answer = answer + " and " + i + " and " + prevNumber + " are amicable";
//                else answer = answer + i + " and " + prevNumber + " are amicable";
//            }

            if (hasBitPatternRepetitions(i, 2)) {
                if (answer.matches("[0-9]+")) answer = answer + " have bit pattern repetitions";
                else answer = answer + " and have bit pattern repetitions";
            }

            if (!answer.matches("[0-9]+")) {
                System.out.println(answer);
//                listView.getItems().add(answer);
            }
        }
    }
}
