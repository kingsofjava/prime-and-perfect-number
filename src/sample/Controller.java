package sample;

//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.scene.control.ListView;
//import javafx.scene.control.TextField;

public class Controller {
/*

    @FXML
    private TextField startField;

    @FXML
    private TextField endField;

    @FXML
    private ListView<String> listView;

    @FXML
    void calculateAction(ActionEvent event) {
        if (startField.getText() == null || startField.getText().isEmpty()) startField.setText("0");
        if (endField.getText() == null || endField.getText().isEmpty()) endField.setText("30");

        int start = Integer.parseInt(startField.getText());
        int end = Integer.parseInt(endField.getText());

//        System.out.println("Range From: " + start + " - " + end);

        listView.getItems().clear();

        int count = 0;
        for (int i = start; i <= end; i++) {
            count++;
//            System.out.println("i: " + i);

            String answer = i + "";
            if (isNumberPrime(i)) {
                answer = answer + " is prime";
            }

            if (isNumberPerfect(i)) {
                if (!answer.contains("is prime")) answer = answer + " is perfect";
                else answer = answer + " and perfect";
            }

            if (count != 1 && isNumberAmicable(i, i - 1)) {
                int prevNumber = i - 1;
                if (answer.matches("[0-9]+")) answer = answer + " and " + i + " and " + prevNumber + " are amicable";
                else answer = answer + i + " and " + prevNumber + " are amicable";
            }

            if (hasBitPatternRepetitions(i, 2)) {
                if (answer.matches("[0-9]+")) answer = answer + " have bit pattern repetitions";
                else answer = answer + " and have bit pattern repetitions";
            }

            if (!answer.matches("[0-9]+")) {
                listView.getItems().add(answer);
            }
        }
    }
*/
    public static boolean isNumberPerfect(int n) {
        if (n == 1) return false;

        int sum = 1;
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0) {
                sum += i;
            }
        }
        return sum == n;
    }


    public static boolean isNumberPrime(int n) {
        if (n < 2) return false;

        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean hasBitPatternRepetitions(int n, int radix) {
        if (n == 0) return false;
        if (n == 1) return false;

        String bitString = Integer.toString(n, radix);
        return bitString.equals(new StringBuilder(bitString).reverse().toString());
    }

    public static boolean isNumberAmicable(int num1, int num2) {
        int sum_num1 = 0, sum_num2 = 0;
        for (int i = 1; i <= num1; i++) {
            if (num1 % i == 0) {
                sum_num1 += i;
            }
        }
        for (int i = 1; i <= num2; i++) {
            if (num2 % i == 0) {
                sum_num2 += i;
            }
        }
        if (sum_num1 == sum_num2){
            return true;
        } else {
            return false;
        }
    }
}
